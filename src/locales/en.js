const en = {
    "Pavel Yakupov":"Pavel Yakupov",
    "Frontend-Developer": "Frontend-Developer",
    "Year of experience": "Year of experience",
    "Summary": "Summary",
    "Volunteer Experience": "Volunteer Experience",
    "Language": "Language",
    "Hobbies and interests": "Hobbies and interests",
    "Skills":"Skills",
    "Work Experience":"Work Experience",
    "Education":"Education",
    "Psychologist": "Psychologist",
    "preview": "Hello, my name is Pavel.Currently i’m living in Tel-Aviv. I got 2.5 year of experience in Frontend-development in Vue3 and React. Except this, i developed backend services in Python, Node.js, Golang and PHP. My first commercial job was about with analytics, graphics, documents - it was Digital Lake. After this i started a career in  Walleti, where i worked with vue3, Nuxt.js and Laravel. Except this, i worked in EdTech - Nordic IT School, Codabra, Ozon Educational. Currently i looking for a new job! Have a nice day."
}
export default en