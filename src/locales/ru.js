const ru = {
    "Pavel Yakupov":"Павел Якупов",
    "Frontend-developer": "Frontend разработчик",
    "Year of experience": "Годы опыта",
    "Summary": "Краткое содержание",
    "Volunteer Experience": "Волонтерский опыт",
    "Language": "Владение языками",
    "Hobbies and interests": "Хобби и интересы",
    "Work Experience":"Опыт работы",
    "Education":"Образование",
    "Psychologist": "Психолог",
    "preview": "Привет, меня зовут Павел. Сейчас я проживаю в Тель-Авиве, и у меня 2.5 года опыта во Frontend-разработке на Vue и React. Кроме этого, я разрабатывал на  Python, Node.js, Golang и PHP. Я занимался сервисом для  аналитики, где работал с графиками, документами, а также загрузкой 3D деталей. После этого я работал в компании Pusk, где занимался криптокошельком и сервисом виртуальных карт - там я в основном использовал Vue3 и Nuxt. Кроме этой деятельности я много работал в EdTech компаниях, таких как Nordic It School, Codara, Ozon Educational. В будущем я бы хотел использовать Vue3, React, Typescript, и Tailwind. Хорошего дня!"

}

export default ru