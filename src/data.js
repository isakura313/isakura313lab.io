const introInfo = {
    name: 'Pavel Yakupov',
    title: 'Frontend-Developer',
    year: 2020,
    location: 'Moscow, Russia',
    description: '<strong>Turpis, sit amet iaculis dui consectetur at.</strong> Cras sagittis molestie orci. <strong>Suspendisse ut laoreet mi</strong>. Phasellus eu tortor vehicula, blandit enim eu, auctor massa. Nulla ultricies tortor dolor, sit amet suscipit enim <strong>condimentum id</strong>. Etiam eget iaculis tellus. Varius sit amet.',
    image: 'ec.jpeg',
}

const petInfo = [
    {
        title: 'Gallery',
        link: 'https://isakura313.gitlab.io/gallery',
        link_gitlab: 'https://gitlab.com/isakura313/gallery',
        description: 'A simple gallery in Nuxt2 with API of Pexels. Was created in the end of 2021, just for fun - i always wanna create gallery.' +
            'You can view popular photos, you can search, and you can create gallery of your favorite images',
    },
    {
        title: 'E-commerce vue',
        link: 'https://isakura313.gitlab.io/gallery',
        link_gitlab: 'https://gitlab.com/isakura313/vite-ecommerce',
        description: 'E-commerce with Vue 3. When i write down a course for Ozon, we create a E-commerce - and i partly re-create it'
    },
    {
        title: 'Ratatype clone',
        link: 'https://isakura313.gitlab.io/keyboard_speed/',
        link_gitlab: 'https://gitlab.com/isakura313/keyboard_speed',
        description: 'A small clone of <a href="https://www.ratatype.ua"> ratatype</a>, if you need to check speed of typing in Russian and English',
    },
    {
        title: 'Weather App',
        link: 'https://isakura313.gitlab.io/weather-app/',
        link_gitlab: 'https://gitlab.com/isakura313/weather-app',
        description: 'Simple weather app with React, Mui, Mobx, that have three page - check weather now, set what city you wanna to know every day, and what time in any city right now',
    },
    {
        title: 'Todo on React Native',
        link_gitlab: 'https://gitlab.com/isakura313/todorn',
        description: 'TODO with React Native, Expo and Typescript',
    }
]
//dynamic icon
const socialMediaInfo = [{
    name: 'linkedin',
    link: 'https://www.linkedin.com/in/paul-yakupov-824008124/',
    username: 'paul-yakupov-824008124',
    displayName: 'linkedin/paul-yakupov-824008124'
},

    {
        name: 'github',
        link: 'https://github.com/isakura313',
        username: 'isakura313',
        displayName: 'github/isakura313'
    },
    {
        name: 'gitlab',
        link: 'https://gitlab.com/isakura313',
        username: 'isakura313',
        displayName: '@isakura313'
    },
    {
        name: 'link',
        username: 'walleti',
        link: 'https://app.walleti.com',
        displayName: 'Walleti'
    },
    {
        name: 'link',
        username: 'pst',
        link: 'https://pst.net',
        displayName: 'PST'
    },
    {
        name: 'link',
        username: 'pusk',
        link: 'https://pusk.ru',
        displayName: 'PUSK'
    },
    // {
    //     name: 'globe',
    //     link: 'http://localhost:8080',
    //     username: 'coskuncayemre',
    //     displayName: 'emrecoskuncay.com'
    // },
    // {
    //     name: 'stack-overflow',
    //     link: 'http://localhost:8080',
    //     username: 'coskuncayemre',
    //     displayName:'stackoverflow.com'
    // },
]


const contactInfo = [
    {
        name: 'envelope',
        context: 'isakura313@gmail.com',
        size: '20px',
        style: "font-size:20px;margin-right: 10px;",
        link: 'mailto:isakura313@gmail.com'
    },
    {
        name: 'phone-square',
        context: '+79197238730',
        size: '23px',
        style: "font-size:23px;margin-right: 10px;",
        link: 'tel:+79197238730'
    },
    {
        name: 'telegram',
        context: '@isakura4',
        size: '23px',
        style: "font-size:23px;margin-right: 10px;",
        link: ''
    },

]

const summaryInfo =
    'preview'


//desc v-html
const experinceInfo = [
    {
        workAt: 'walleti, pst.net, paywallet, pusk.ru',
        position: 'Frontend-Developer',
        duration: 'March 2022 – Present',
        description: 'Work on crypto HD Wallet with React, React Native, Vue  - mobile version and desktop. I start React Native version of CryptoWallet from zero, then working with mobile, desktop version.  ',
        techs: ["Vue 3", "React", "React Native", "Android", "Xcode", "Crypto", "HD Wallet", "IOS", "Crypto", "Typescript", "Webstorm"]
    },
    {
        workAt: 'SoftWare-KIT',
        position: 'Frontend-Developer',
        duration: 'December 2020 – March 2022',
        description: 'Work as Vue Frontend Developer for Digital Lake. It`s analytics platform, like Prometheus Grafana and Redash. I work with documents edition, graphics and data and tables. I used Vuetify for components, Vue ChartJS from diagrams. The main challenge was to work with reactive update of data from the lake ',
        techs: ["Vue.JS", "Python", "Docker", "Vuex", "Vuetify", "JS", "Chart.js", "Vuetify"]
    },
    {
        workAt: 'Codabra',
        position: 'IT-teacher',
        duration: 'September 2018 - May 2021',
        description: 'Partly working as IT Teacher, i taught Minecraft, Web-development, Unity ',
        techs: ["JS", "Minecraft", "C#"]
    },
    {
        workAt: 'Ozon',
        position: 'Metodist',
        duration: 'March 2020 – May 2021',
        description: 'i write a course for Ozon about Vue.js, and create course about Python',
        techs: ["VUE.JS", "Metodics", " Python"]
    },
    {
        workAt: 'OTUS',
        position: 'Writer',
        duration: 'September 2019 – March 2020',
        description: 'I writed an articles for Habr.ru. It was articles about different technology,' +
            'Laravel, Django, JavaScript, Golang',
        techs: ["Swift", "Django", "Node.js", "JavaScript", 'writer']
    },
    {
        workAt: 'Nordic IT School',
        position: 'IT - teacher',
        duration: 'March 2018 – Present',
        description: 'I taughted Web-development students in IT-school',
        techs: ['HTML', 'CSS', 'JS', 'PHP']
    }
]

const educationInfo = [
    {
        organisation: 'Moscow Pedagogical State Universitet ',
        title: 'Psychologist',
        duration: '2009 – 2014',
        description: '',
    }, {
        organisation: 'State University of Management',
        title: 'Learn.Javascript.ru',
        duration: '2017 – 2021',
        description: 'Course about React, JavaScript, Typescript',
    },
]

const volunteerInfo = [
    {
        organisation: 'Course for Belorussian programmers',
        title: 'IT Teacher',
        duration: 'june 2020 – jule 2020',
        description: '',
    },
]


const certificateInfo = [
    {
        organisation: 'learn.javascript.ru',
        title: 'JavaScript',
        duration: 'October 2018',
        description: '',
    },
    {
        organisation: 'learn.javascript.ru',
        title: 'React.js',
        duration: 'September 2019',
        description: '',
    },
]

const projectInfo = [{
    title: 'Project 1',
    meta: 'Propulsion Engineer',
    description: 'Responsibilities included: writing technical reports and other documentation, such as handbooks and bulletins, for use by engineering staff, management, and customers, analyzing project requests and proposals and engineering data to determine feasibility, predictability, cost, and production time of aerospace or aeronautical product.',
    images: [{
        thumbnail: 'http://placeholdit.imgix.net/~text?txtsize=33&txt=firstimage&w=150&h=150',
        original: 'http://placeholdit.imgix.net/~text?txtsize=33&txt=firstimage&w=900&h=900'
    },
        {
            thumbnail: 'http://placeholdit.imgix.net/~text?txtsize=33&txt=secondimage&w=150&h=150',
            original: 'http://placeholdit.imgix.net/~text?txtsize=33&txt=secondimage&w=900&h=900'
        },
        {
            thumbnail: 'http://placeholdit.imgix.net/~text?txtsize=33&txt=thirdImage&w=150&h=150',
            original: 'http://placeholdit.imgix.net/~text?txtsize=33&txt=thirdImage&w=900&h=900'
        }
    ]
},
    {
        title: 'Project 1',
        meta: 'Propulsion Engineer',
        description: 'Responsibilities included: writing technical reports and other documentation, such as handbooks and bulletins, for use by engineering staff, management, and customers, analyzing project requests and proposals and engineering data to determine feasibility, predictability, cost, and production time of aerospace or aeronautical product.',
        images: [{
            thumbnail: 'http://placeholdit.imgix.net/~text?txtsize=33&txt=firstimage&w=150&h=150',
            original: 'http://placeholdit.imgix.net/~text?txtsize=33&txt=firstimage&w=900&h=900'
        },
            {
                thumbnail: 'http://placeholdit.imgix.net/~text?txtsize=33&txt=secondimage&w=150&h=150',
                original: 'http://placeholdit.imgix.net/~text?txtsize=33&txt=secondimage&w=900&h=900'
            },
            {
                thumbnail: 'http://placeholdit.imgix.net/~text?txtsize=33&txt=thirdImage&w=150&h=150',
                original: 'http://placeholdit.imgix.net/~text?txtsize=33&txt=thirdImage&w=900&h=900'
            }
        ]
    }
]


const skillInfo = [
    {
        name: 'HTML/CSS',
        rate: 85,
    },
    {
        name: 'JavaScript',
        rate: 90,
    },
    {
        name: 'Vue.js',
        rate: 85,
    },
    {
        name: 'React',
        rate: 80,
    },
    {
        name: 'React Native',
        rate: 75,
    },
    {
        name: 'Mobx',
        rate: 85,
    },
    {
        name: 'Docker',
        rate: 70,
    },
    {
        name: 'Python',
        rate: 70
    }

]

const otherSkillInfo = [ 'Nginx', 'Git', 'MVC', 'Bootstrap', 'Jira', 'Vuetify', 'i18']

const proSkillInfo = ['Leadership', 'Effective communication', 'Team player', 'Strong problem solver',]


const languageInfo = [{
    name: 'Czech',
    rate: 1,
    level: 'Begginer',
},
    {
        name: 'English',
        rate: 4,
        level: 'Adv',
    },
    {
        name: 'Russian',
        rate: 5,
        level: 'Adv',
    },
    {
        name: 'Hebrew',
        rate: 2,
        level: 'Beg',
    },
]


const interestInfo = [{
    name: 'Linux',
    description: "I love working with Unix",
},
    {
        name: 'Streams',
        description: "Do educational content and make streams",
    },
]

export default {
    introInfo: introInfo,
    socialMediaInfo: socialMediaInfo,
    experinceInfo: experinceInfo,
    educationInfo: educationInfo,
    projectInfo: projectInfo,
    skillInfo: skillInfo,
    certificateInfo: certificateInfo,
    volunteerInfo: volunteerInfo,
    languageInfo: languageInfo,
    contactInfo: contactInfo,
    interestInfo: interestInfo,
    summaryInfo: summaryInfo,
    otherSkillInfo: otherSkillInfo,
    proSkillInfo: proSkillInfo,
    petInfo: petInfo
}